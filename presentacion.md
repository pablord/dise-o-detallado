# Introducción

## Introducción [1/2]

* Componentes para el desarrollo de SismObserver.
* Relaciones entre ellos.
* Selección de Escenarios con Diagramas de Secuencia.
* Tecnologías involucradas.
* Métricas de Gestión.

## Introducción [2/2]

* Roles:
	* Bryan Guzmán: Experto en Base de Datos MongoDB y Lucene.
	* Cristian Jeldes: Experto en Web Services y J2EE.
	* Katherine Liberona: Experta en aplicación móvil para el sistema Android.
	* Robinson Oyarzún: Jefe de proyecto. Experto en métricas de gestión y QA.
	* Maximiliano Pérez: Experto en Framework Django y HTML5.
	* Pablo Reyes: Experto en análisis de sentimientos con Weka.

# Vista Lógica
<!--## Vista Lógica [/]-->

<!--* Se presentan los módulos del sistema.-->
<!--	* Manejo de Tweets.-->
<!--	* Manejo de Gráficos.-->
<!--	* Manejo de Mapas.-->
<!--	* Módulo de Estadísticas.-->

## Vista Lógica [1/2]

**Manejo de Tweets**

* Gestión de la información.
* Creación de una nube de Tweets.

**Manejo de Gráficos**

* Genera y muestra los gráficos al usuario.

**Manejo de Mapas**

* Genera y muestra los mapas de Tweets y de calor al usuario.

**Módulo de Estadísticas**

* Estadísticas generadas a partir de los Tweets.

## Vista Lógica [2/2]

![Diagrama de Componentes.](imagenes/VL.png)

# Vista de Proceso

## Vista de Proceso [1/2]

* Diagrama de Despliegue con los componentes físicos del sistema.
	* 4 máquinas que soportan a los ejecutables.

## Vista de Proceso [2/2]

![Diagrama de Despliegue.](imagenes/VP.png)

# Vista de Desarrollo

## Vista de Desarrollo [1/2]

* Se centra en la organización de los componentes del software que son desarrollados
de forma independiente.
* Separación en subsistemas.
* Capas del sistema:
	* **Capa de Datos**.
		* Se encarga de gestionar la información con la que trabaja el sistema.
	* **Capa de Negocio**.
		* Se encarga de entregar la información a la página web y a la aplicación móvil.
		* En esta capa se consume el Web Service.
		* Análisis de sentimiento de Tweets.
	* **Capa de Presentación**.
		* El usuario interactúa con esta capa, mediante la conexión que posee con la capa de negocio.
		* En esta capa se realizan las consultas al Web Service.

## Vista de Desarrollo [2/2]

![Diagrama de Desarrollo.](imagenes/VD.png)

# Escenarios

## Escenarios [1/16]

1. **Ver mapa de Tweets respecto a sismos a nivel nacional.**
	* Actores: Usuario Web y Usuario Android.
	* Pre-condiciones:
		* El sistema ha separado los Tweets que están geolocalizados de los que no.
	* Excepciones:
		* El usuario no selecciona las fechas. **Solución:** Se muestra un mensaje por pantalla y se indica que ingrese las fechas.
		* El sistema no responde a la consulta. **Solución:** Se le informa al usuario que ha ocurrido un imprevisto y se solicita que vuelva a ingresar.
		* No hay Tweets geolocalizados. **Solución:** Se le indicará al usuario que no puede realizar esta consulta en la fecha seleccionada.

## Escenarios [2/16]

![Escenario 1 Web.](imagenes/e1w.png)

## Escenarios [3/16]

![Escenario 1 Android.](imagenes/e1a.png)

## Escenarios [4/16]

6. **Ver comparación de mapas de calor de Clasificación de Sentimiento por región.**
	* Actores: Usuario Web.
	* Pre-condiciones:
		* El sistema ha separado los Tweets que están geolocalizados con los que no.
		* El sistema ha clasificado los Tweets de acuerdo sus sentimientos.
	* Post-Condiciones:
		* El sistema registra la visita realizada por el usuario.

## Escenarios [5/16]

* Excepciones:
	* El usuario no selecciona las fechas. **Solución:** se muestra un mensaje por pantalla y se solicita que ingrese las fechas.
	* El sistema no responde a la consulta. **Solución:** Se le informa al usuario que ha ocurrido un imprevisto y se solicita que vuelva a ingresar.
	* No hay Tweets geolocalizados. **Solución:** Se le indicará al usuario que no puede realizar consultas en aquella región.

## Escenarios [6/16]

![Escenario 6 Web.](imagenes/e6w.png)

## Escenarios [7/16]

10. **Ver gráficos de Clasificación de Sentimiento a nivel nacional.**
	* Actores: Usuario Web y Usuario Android.
	* Pre-condiciones:
		* El sistema ha separado los Tweets que están geolocalizados con los que no.
		* El sistema ha clasificado los Tweets de acuerdo sus sentimientos.
	* Post-Condiciones:
		* El sistema registra la visita realizada por el usuario.

## Escenarios [8/16]

* Excepciones:
	* El usuario no selecciona las fechas. **Solución:** se muestra un mensaje por pantalla y se solicita que ingrese las fechas.
	* El sistema no responde a la consulta. **Solución:** Se le informa al usuario que ha ocurrido un imprevisto y se solicita que vuelva a ingresar.
	* No hay Tweets geolocalizados. **Solución:** Se le indicará al usuario que no puede realizar esta consulta en la fecha seleccionada.

## Escenarios [9/16]

![Escenario 10 Web.](imagenes/e10w.png)

## Escenarios [10/16]

![Escenario 10 Android.](imagenes/e10a.png)

## Escenarios [11/16]

14. **Ver Tweets, que resulten luego de seleccionar un elemento de la nube de palabras.**
	* Actores: Usuario Web y Usuario Android.
	* Pre-condiciones:
		* El sistema ha separado los Tweets que están geolocalizados con los que no.
		* El sistema ha generado índices invertidos de los tweets geolocalizados.
	* Post-Condiciones:
		* El sistema registra la visita realizada por el usuario.
	* Excepciones:
		* El usuario no selecciona las fechas. **Solución:** se muestra un mensaje por pantalla y se solicita que ingrese las fechas.
		* El sistema no responde a la consulta. **Solución:** Se le informa al usuario que ha ocurrido un imprevisto y se solicita que vuelva a ingresar.

## Escenarios [12/16]

![Escenario 14 Web.](imagenes/e14w.png)

## Escenarios [13/16]

![Escenario 14 Android.](imagenes/e14a.png)

## Escenarios [14/16]

16. **Ver mapa de calor de Clasificación de Sentimiento usando la geolocalización actual.**
	* Actores: Usuario Android.
	* Pre-condiciones:
		* El sistema ha separado los Tweets que están geolocalizados con los que no.
		* El sistema ha detectado la geolocalización actual del dispositivo.
	* Post-Condiciones:
		* El sistema registra la visita realizada desde la aplicación.

## Escenarios [15/16]

* Excepciones:
	* El usuario no selecciona las fechas **Solución:** se muestra un mensaje por pantalla y se solicita que ingrese las fechas
	* El sistema no responde a la consulta. **Solución:** Se le informa al usuario que ha ocurrido un imprevisto y se solicita que vuelva a ingresar
	* No hay Tweets geolocalizados. **Solución:** Se le indicará al usuario que no puede realizar esta consulta en la fecha seleccionada.

## Escenarios [16/16]

![Escenario 16 Android.](imagenes/e16a.png)

# Tecnologías

## Tecnologías [1/9]

* **Android 4.4.4 KitKat**
	* Se utilizará para la creación de una aplicación móvil de SismObserver.
	* Alrededor del 33.3% (2013-2014) de smartphones en el mundo utilizan Android.
	* Permite conexión con API de GoogleMaps, la cual será favorable para el proyecto.

![Android.](imagenes/android.png)

## Tecnologías [2/9]

* **Django 1.7**
	* Framework de desarrollo integrado.
	* Trabaja bajo el concepto de M-V-T.
	* Permite conexión ordenada entre plantillas de HTML5.

![Django.](imagenes/django.png)

## Tecnologías [3/9]

* **HTML5 - CSS3 - JS**
	* Lenguaje para fabricación de páginas web.
	* Permite realizar diseños de tamaños adaptables junto con JS y CSS.
	* Permite trabajar con la API GoogleMaps.
	* Permite generar gráficos.

![HTML5 - CSS3 - JS.](imagenes/html5.png)

## Tecnologías [4/9]

* **WebService RESTful en Java EE , Versión Java Platform Enterprise Edition 7**
	* Plataforma usada para desarrollar aplicaciones web.
	* Basado en Java.
	* Permite establecer una interfaz única para acceder a las funcionalidades, tanto móvil como web.
	* Integración con Lucene, ya que se basa en Java.

![WebService en Java EE.](imagenes/java.png)

## Tecnologías [5/9]

* **Lucene 4.10.1**
	* Biblioteca de software.
	* Soporta indexaciones incrementales.
	* Mejora tiempos de búsqueda en la BD.

![Lucene.](imagenes/lucene.png)

## Tecnologías [6/9]

* **MongoDB 2.6.4**
	* Sistema de base de datos noSQL
	* Consultas:
		* Con grandes volúmenes de datos.
		* Geoespaciales.
	* Base de datos orientada a documentos.

![MongoDB.](imagenes/mongo.png)

## Tecnologías [7/9]

* **Weka 3.6.8**
	* Librerías implementadas en Java.
	* Aprendizaje supervisado.
	* Minería de datos.
	* Clasificación de grandes volúmenes de información.

![Weka.](imagenes/weka.png)

## Tecnologías [8/9]

* **Selenium 2.8.0**
	* Utilizado en la etapa de QA.
	* Utilizado para pruebas a la aplicación web.
	* Detectar errores, conociendo su ubicación.

![Selenium.](imagenes/selenium.png)

## Tecnologías [9/9]

* **JUnit 4.11**
	* Entorno conformado por un conjunto de bibliotecas.
	* Utilizado en la etapa de QA.
	* Realizar test unitarios a los métodos del sistema.
	* Automatiza el proceso de testing de las funcionalidades.

![JUnit.](imagenes/junit.png)

# Herramientas

## Herramientas [1/3]

* **Herramientas de trabajo**
	* Trello: Herramienta de organización de proyectos.
	* Google Drive: Nube virtual de Google.
	* Google Hangout: Herramienta de comunicación de Google.

![Trello - Google Drive - Google Hangout.](imagenes/herramientas.jpg)

## Herramientas [2/3]

![Trello.](imagenes/trello.png)

## Herramientas [3/3]

![Google Drive.](imagenes/drive.png)

# Hardware

## Hardware [1/1]

* **2 máquinas para GlassFish 4.1 y Apache 2.4.1**
	* 1 Procesador Intel Xeon E5620.
	* 1 disco duro de 320 GB de capacidad a 1000 rpm.
	* 16 GB de memoria RAM DDR3 1066MHz.

* **2 máquinas para MongoDB 2.6**
	* 1 Procesador Intel Xeon E5620.
	* 1 disco duro SSD de 256 GB de capacidad.
	* 8 GB de memoria RAM DDR3 1066MHz.

**Todas las máquinas corriendo con Linux Mint Debian Edition 17 de 64 bit sin entorno gráfico.**

# Planificación

## Planificación [1/1]

* **Carta Gantt**

![Carta Gantt Diseño Arquitectural.](imagenes/cartagantt.png)

# Métricas de Gestión

## Métricas de Gestión [1/4]

* **Burn Down**

![Burn Down.](imagenes/burndown.png)

## Métricas de Gestión [2/4]

* **Burn Up**

![Burn Up.](imagenes/burnup1.png)

## Métricas de Gestión [3/4]

![Burn Up.](imagenes/burnup2.png)

## Métricas de Gestión [4/4]

![Burn Up.](imagenes/burnup3.png)

# Minutas

## Minutas [1/7]

* **Reunión Retrospectiva**

![Reunión Retrospectiva.](imagenes/retrospectiva1.png)

## Minutas [2/7]

![Reunión Retrospectiva.](imagenes/retrospectiva2.png)

## Minutas [3/7]

![Foto Reunión Retrospectiva.](imagenes/foto1.jpg)

## Minutas [4/7]

![Foto Reunión Retrospectiva.](imagenes/foto2.jpg)

## Minutas [5/7]

* **Reunión Diaria**

![Reunión Diaria.](imagenes/diaria1.png)

## Minutas [6/7]

![Reunión Diaria.](imagenes/diaria2.png)

## Minutas [7/7]

![Reunión Diaria.](imagenes/diaria3.png)

# Conclusiones

## Conclusiones [1/1]

* Importancia de conocer los componentes y la relación entre éstos.
* Se espera que el cliente conozca las tecnologías a utilizar.
* Se conoce la arquitectura que tendrá el sistema a desarrollar.
* Se dejan expresadas las bases para el posterior diseño detallado.

## Gracias por su atención

\begin{center}
\LARGE{¿Preguntas?}
\end{center}
