\beamer@endinputifotherversion {3.24pt}
\select@language {spanish}
\beamer@sectionintoc {1}{Introducci\IeC {\'o}n}{3}{0}{1}
\beamer@sectionintoc {2}{Vista L\IeC {\'o}gica}{5}{0}{2}
\beamer@sectionintoc {3}{Vista de Proceso}{7}{0}{3}
\beamer@sectionintoc {4}{Vista de Desarrollo}{9}{0}{4}
\beamer@sectionintoc {5}{Escenarios}{11}{0}{5}
\beamer@sectionintoc {6}{Tecnolog\IeC {\'\i }as}{27}{0}{6}
\beamer@sectionintoc {7}{Herramientas}{36}{0}{7}
\beamer@sectionintoc {8}{Hardware}{39}{0}{8}
\beamer@sectionintoc {9}{Planificaci\IeC {\'o}n}{40}{0}{9}
\beamer@sectionintoc {10}{M\IeC {\'e}tricas de Gesti\IeC {\'o}n}{41}{0}{10}
\beamer@sectionintoc {11}{Minutas}{45}{0}{11}
\beamer@sectionintoc {12}{Conclusiones}{52}{0}{12}
